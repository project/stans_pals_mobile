CONTENTS OF THIS FILE
---------------------
   
 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers

INTRODUCTION
------------

Stans Pals Mobile is a Subtheme of the Stans Pals. While both have dynamic
scaling elements, Stans Pals Mobile focuses more on styling appropriate to
a mobile site and a touch interface. The theme will scale dynamically from
1920px wide down to 160px wide changing elements in steps as the window
size reduces or the device is swapped between portrait and landscape

 * For a full description of the module, visit the project page:
   https://www.drupal.org/sandbox/bigmonmulgrew/2547771

 * To submit bug reports and feature suggestions, or to track changes:
   https://drupal.org/project/issues/2547771
   
REQUIREMENTS
------------

This theme requires the following parent theme:

 * Business Theme (https://www.drupal.org/project/business)

This theme may optionally be supported by:

 * Stans Pals Mobile (https://www.drupal.org/sandbox/bigmonmulgrew/2547829)
 
 INSTALLATION
------------
 
 * Install as you would normally install a contributed Drupal theme. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.   
   
CONFIGURATION
-------------

 * To enable the theme go to http://yoursite.co.uk/admin/appearance 
 * Scroll down to locat the theme.
 * Click Enable and Set Default
 
MAINTAINERS
-----------

Current maintainers:
 * David Mulgrew (bigmonmulgrew) - https://drupal.org/u/bigmonmulgrew

This project has been sponsored by:
 * MULGREW ENTERPRISES
   Specialized in IT management, consulting and planning.
   Visit http://foreverythingit.co.uk for more information.
