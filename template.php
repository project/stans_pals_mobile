<?php
/**
 * @file
 * Contains theme functions for the Stans Pals Mobile theme.
 *
 * See the theme project page for more information:
 *   https://www.drupal.org/project/stans_pals_mobile .
 */

/**
 * Builds the page.
 *
 * Don't forget to extend this once its finished ;D.
 */
function stans_pals_mobile_preprocess_html(&$vars) {
  $viewport = array(
    '#tag' => 'meta',
    '#attributes' => array(
      'name' => 'viewport',
      'content' => 'width=device-width, initial-scale=1',
    ),
  );

  drupal_add_html_head($viewport, 'viewport');
}
